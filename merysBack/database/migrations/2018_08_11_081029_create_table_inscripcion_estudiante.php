<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInscripcionEstudiante extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcion_estudiante', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iddetalle')->unsigned();
            $table->integer('idestudiante')->unsigned();
            $table->boolean('active');
            $table->foreign('iddetalle')->references('id')->on('inscripcion_detalle');
            $table->foreign('idestudiante')->references('id')->on('estudiantes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcion_estudiante');
    }
}
