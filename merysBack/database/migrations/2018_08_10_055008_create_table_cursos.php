<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idtipocurso')->unsigned();
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->integer('cantidad_horas_practicas');
            $table->integer('cantidad_horas_teoricas');
            $table->double('costo');
            $table->boolean('active')->default(1);
            $table->foreign('idtipocurso')->references('id')->on('tipos_cursos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
