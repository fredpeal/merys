<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInscripcionDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcion_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idinscripcion')->unsigned();
            $table->integer('idprofesor')->unsigned();
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->integer('cantidad_estudiante');
            $table->integer('cantidad_actual');
            $table->date('fecha_inicio');
            $table->foreign('idinscripcion')->references('id')->on('inscripcion');
            $table->foreign('idprofesor')->references('id')->on('profesores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcion_detalle');
    }
}
