import Vue from 'vue'
import App from './App.vue'

import { rutas } from './rutas.js';
 import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  routes:rutas,
  mode:'history'
});

new Vue({
  el: '#app',
  router:router,
  render: h => h(App)
})
