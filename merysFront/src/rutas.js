 import Contenido from './assets/components/includes/content.vue'
 import ContenedorCursos from './assets/components/cursos/index.vue'
 
 //Tipos de cursos
 import TiposCursos from './assets/components/cursos/tiposCuros/index.vue';
 import TiposCursosAgregar from './assets/components/cursos/tiposCuros/create.vue';

 //Cursos
 import CursosIndex from './assets/components/cursos/cursos/index.vue';
 import CursosCreate from './assets/components/cursos/cursos/create.vue';


export const rutas = [
    {path:'/contenido',component:Contenido,name:'contenido'},
    {path:'/contenidoCursos',component:ContenedorCursos,name:'contenidoCursos'},

    //Tipos de Cursos
    {path:'/tipos-cursos',component:TiposCursos,name:'tipos-cursos'},
    {path:'/tipos-cursos-agregar',component:TiposCursosAgregar,name:'tipos-cursos-agregar'},
    
    //Cursos Create
    {path:'/cursos',component:CursosIndex,name:'cursos'},
    {path:'/cursos-create',component:CursosCreate,name:'cursos-create'}
]